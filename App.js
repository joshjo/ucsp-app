import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Button,
} from 'react-native';


import Input from './src/input';



export default function App() {
  const [minNota, setMinNota] = React.useState('11.5');
  const [notaFinal, setNotaFinal] = React.useState(null);

  function submit() {
  }

  return (
    <React.Fragment>
      <View style={styles.container}>
        <View style={{ alignContent: 'center', justifyContent: 'center', marginTop: 20 }}>
          <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'center' }}>Calculadora UCSP</Text>
        </View>
        <View>
          <Input label="Aprobar con" value={minNota} onChangeText={setMinNota} />
          <Button title="Calcular" onPress={submit} />
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 20 }}>
          {notaFinal !== null && (
            <React.Fragment>
              {notaFinal > 0 ? (
                <Text>Caso A</Text>
              ) : (
                <Text>Caso B</Text>
              )}
            </React.Fragment>
          )}
        </View>
      </View>

    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    backgroundColor: '#ffffee',
    width: '100%',
    marginVertical: 10,
    flexDirection: 'row',
  },
  column: {
    flex: 1,
    height: '100%',
    backgroundColor: 'blue',
    marginHorizontal: 10,
  },
  container: {
    padding: 25,
  },
});
